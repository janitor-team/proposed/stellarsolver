Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: stellarsolver
Source: https://github.com/rlancaste/stellarsolver

Files: *
Copyright: Robert Lancaster, 2020
License: GPL-2+

Files: cmake/*
Copyright: 2006, 2016, Jasem Mutlaq <mutlaqja@ikarustech.com>
           2014, Kelly Thompson <kgt@lanl.gov>
           2007-2009, Kitware, Inc.
           2020, Robert Lancaster <rlancaste@gmail.com>
License: BSD-3-clause

Files: stellarsolver/astrometry/*
Copyright: 2006-2015 astrometry.net, Dustin Lang, Keith Mierle, David W. Hogg,
 Gerard Jungman, Michael Blanton, Denis Vida, Brian Grough, Sam Roweis,
 Christopher Stumm, David Warde-Farley, Michal Kočer, Tamas Budavari
 2008 B.A. Weaver,
 1992, 1993 The Regents of the University of California,
 1970-2003, Wm. Randolph Franklin,
 2002-2007 Gene Cash,
 2007-2008 Ianaré Sévi
 2015 Ole Streicher <olebole@debian.org> (debian)
License: BSD-3-clause

Files: stellarsolver/astrometry/blind/windirent.h
Copyright: 1998-2019, Toni Ronkko
License: dirent-MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Files: stellarsolver/astrometry/include/astrometry/anqfits.h
       stellarsolver/astrometry/include/astrometry/qfits_byteswap.h
       stellarsolver/astrometry/include/astrometry/qfits_card.h
       stellarsolver/astrometry/include/astrometry/qfits_error.h
       stellarsolver/astrometry/include/astrometry/qfits_float.h
       stellarsolver/astrometry/include/astrometry/qfits_header.h
       stellarsolver/astrometry/include/astrometry/qfits_image.h
       stellarsolver/astrometry/include/astrometry/qfits_md5.h
       stellarsolver/astrometry/include/astrometry/qfits_memory.h
       stellarsolver/astrometry/include/astrometry/qfits_rw.h
       stellarsolver/astrometry/include/astrometry/qfits_std.h
       stellarsolver/astrometry/include/astrometry/qfits_table.h
       stellarsolver/astrometry/include/astrometry/qfits_time.h
       stellarsolver/astrometry/include/astrometry/qfits_tools.h
Copyright: 2001-2004, European Southern Observatory
           2007-2013, Dustin Lang
License: GPL-2+

Files: stellarsolver/astrometry/libkd/an-fls.h
Copyright: 1990, 1993, The Regents of the University of California.  All rights reserved.
License: BSD-3-clause

Files: stellarsolver/astrometry/qfits-an/*
Copyright: Nicolas Devillard <ndevilla@gmail.com>
           2001-2004, European Southern Observatory
           2002, Yves Jung <yjung@eso.org>
           2007, 2010, 2013, Dustin Lang
License: GPL-2+

Files: stellarsolver/astrometry/util/md5.c
Copyright: 2001-2003, Christophe Devine
License: GPL-2+

Files: stellarsolver/sep/*
Copyright: 1993-2011, Emmanuel Bertin
           2014, SEP developers
License: LGPL-3+

Files: stellarsolver/sep/overlap.h
       stellarsolver/sep/util.cpp
Copyright: Thomas Robitaille
           Kyle Barbary
License: BSD-3-clause

Files: ssolverutils/bayer.c
       ssolverutils/bayer.h
Copyright: Damien Douxchamps
           Frederic Devernay
           Dave Coffin
License: LGPL-2.1+

Files: ssolverutils/dms.cpp
       ssolverutils/dms.h
       tester/nan.h
Copyright: 2001, Jason Harris <jharris@30doradus.org>
           2013, Akarsh Simha <akarsh.simha@kdemail.net>
License: GPL-2+

Files: debian/*
Copyright: 2021-2022, Pino Toscano <pino@debian.org>
License: LGPL-2.1+

License: BSD-3-clause
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 On Debian systems, the full text of the GNU Lesser General Public License
 version 2.1 can be found in the file `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-3+
 On Debian systems, the full text of the GNU Lesser General Public License
 version 3 can be found in the file `/usr/share/common-licenses/LGPL-3'.
